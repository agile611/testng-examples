package com.itnove.testng.examples.listenersapi;

import com.itnove.testng.examples.suitelistener.SuiteListener;
import org.testng.TestNG;


public class TestNGListenersAPI {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.setTestClasses(new Class[]{TestClass.class});
        testNG.addListener(new SuiteListener());
        testNG.run();
    }
}
