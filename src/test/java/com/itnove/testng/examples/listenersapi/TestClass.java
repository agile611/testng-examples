package com.itnove.testng.examples.listenersapi;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class TestClass {
    @BeforeSuite
    public void beforeSuite() {
        System.out.println("before suite");
    }

    @Test
    public void t() {
        System.out.println("in test method t");
    }
}
