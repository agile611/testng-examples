package com.itnove.testng.examples.suitelistener;

import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        System.out.println("Start suite " + suite.getName());
    }

    @Override
    public void onFinish(ISuite suite) {
        System.out.println("Finish suite " + suite.getName());
    }
}
