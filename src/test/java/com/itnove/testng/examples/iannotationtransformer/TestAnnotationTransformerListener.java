package com.itnove.testng.examples.iannotationtransformer;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import com.itnove.testng.examples.dataproviderannotationtransformer.DataProviderFactory;
import org.testng.IAnnotationTransformer2;
import org.testng.annotations.IConfigurationAnnotation;
import org.testng.annotations.IDataProviderAnnotation;
import org.testng.annotations.IFactoryAnnotation;
import org.testng.annotations.ITestAnnotation;

public class TestAnnotationTransformerListener implements IAnnotationTransformer2 {

    @Override
    public void transform(ITestAnnotation annotation, Class testClass,
                          Constructor testConstructor, Method testMethod) {
        if (testMethod.getName().equals("t1")) {
            System.out.println("set data provider for " + testMethod.getName());
            annotation.setDataProviderClass(DataProviderFactory.class);
            annotation.setDataProvider("getDp1");
        } else if (testMethod.getName().equals("t2")) {
            System.out.println("set data provider for " + testMethod.getName());
            annotation.setDataProviderClass(DataProviderFactory.class);
            annotation.setDataProvider("getDp2");
        } else if (testMethod.getName().equals("t3")) {
            System.out.println("Disable " + testMethod.getName());
            annotation.setEnabled(false);
        } else if (testMethod.getName().equals("t4")) {
            System.out.println("Increase the timeout for " + testMethod.getName());
            annotation.setTimeOut(200);
        }
    }

    @Override
    public void transform(IDataProviderAnnotation annotation, Method method) {
    }

    @Override
    public void transform(IFactoryAnnotation annotation, Method method) {
    }

    @Override
    public void transform(IConfigurationAnnotation annotation, Class testClass,
                          Constructor testConstructor, Method testMethod) {
    }
}
