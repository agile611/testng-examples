package com.itnove.testng.examples.iannotationtransformer;

import org.testng.annotations.Test;

public class TestAnnotationTransformerExample {
	
	@Test
	public void t1(String param) {
		System.out.println("Method is t1, parameter is " + param);
	}
	
	@Test
	public void t2(String param) {
		System.out.println("Method is t2, parameter is " + param);
	}
	
	@Test
	public void t3() {
		System.out.println("Method is t3");
	}
	
	@Test(timeOut=100)
	public void t4() {
		System.out.println("Method is t4");
		try {
			Thread.sleep(101);
		} catch (InterruptedException e) {
			System.out.println("Sleep interrupted: " + e);
		}
	}
}
